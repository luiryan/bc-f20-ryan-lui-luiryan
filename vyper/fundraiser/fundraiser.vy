# @version ^0.2.4
struct Contributor:
    userAddress: address
    contribution: uint256

owner: public(address)
target: public(uint256)
endTime: public(uint256)
contributors: Contributor[5]
numContributors: int128

@external
def __init__(_target: uint256, duration: uint256):
    self.owner = msg.sender
    self.target = _target
    self.endTime = block.timestamp + duration
    self.numContributors = 0

@external
@payable
def contribute():
    assert(block.timestamp < self.endTime)
    assert(self.numContributors < 5)
    self.contributors[self.numContributors].userAddress = msg.sender
    self.contributors[self.numContributors].contribution = msg.value
    self.numContributors += 1

@external
def collect():
    assert(self.balance >= self.target)
    assert(msg.sender == self.owner)
    selfdestruct(self.owner)

@external
def refund():
    assert(block.timestamp > self.endTime)
    assert(self.balance < self.target)
    for i in range(5):
        if self.contributors[i].contribution != 0:
            send(self.contributors[i].userAddress, self.contributors[i].contribution)

@external
@view
def v_balance() -> uint256:
    return self.balance


