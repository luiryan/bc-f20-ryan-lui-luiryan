/* ================================================================================*/
/* Javascript code for Gacha DApp
/* ================================================================================*/

/* Check if Metamask is installed. */
if (typeof window.ethereum !== 'undefined') {
    console.log('MetaMask is installed!');
} else {
    console.log('Please install MetaMask or another browser-based wallet');
}

/* Instantiate a Web3 client that uses Metamask for transactions.  Then,
 * enable it for the site so user can grant permissions to the wallet */
const web3 = new Web3(window.ethereum);
window.ethereum.enable();

/* Grab ABI from compiled contract (e.g. in Remix) and fill it in.
 * Grab address of contract on the blockchain and fill it in.
 * Use the web3 client to instantiate the contract within program */
var GachaABI = [{"outputs":[],"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"name":"register","outputs":[],"inputs":[],"stateMutability":"payable","type":"function","gas":148625},{"name":"buyTicket","outputs":[],"inputs":[{"type":"uint256","name":"tickets"}],"stateMutability":"payable","type":"function","gas":48033},{"name":"summon","outputs":[],"inputs":[],"stateMutability":"nonpayable","type":"function","gas":266177},{"name":"fight","outputs":[],"inputs":[{"type":"uint256","name":"playerServantNum"},{"type":"uint256","name":"enemyServantNum"}],"stateMutability":"nonpayable","type":"function","gas":435264},{"name":"owner","outputs":[{"type":"address","name":""}],"inputs":[],"stateMutability":"view","type":"function","gas":1361},{"name":"classList","outputs":[{"type":"string","name":"name"}],"inputs":[{"type":"uint256","name":"arg0"}],"stateMutability":"view","type":"function","gas":7182},{"name":"currPlayers","outputs":[{"type":"uint256","name":""}],"inputs":[],"stateMutability":"view","type":"function","gas":1421},{"name":"playerList","outputs":[{"type":"address","name":""}],"inputs":[{"type":"uint256","name":"arg0"}],"stateMutability":"view","type":"function","gas":1560},{"name":"cooldown","outputs":[{"type":"uint256","name":""}],"inputs":[{"type":"address","name":"arg0"}],"stateMutability":"view","type":"function","gas":1635},{"name":"servants","outputs":[{"type":"tuple","name":"cl","components":[{"type":"string","name":"name"}]},{"type":"uint256","name":"level"},{"type":"uint256","name":"atk"},{"type":"uint256","name":"hp"}],"inputs":[{"type":"address","name":"arg0"},{"type":"uint256","name":"arg1"}],"stateMutability":"view","type":"function","gas":11268},{"name":"servantCount","outputs":[{"type":"uint256","name":""}],"inputs":[{"type":"address","name":"arg0"}],"stateMutability":"view","type":"function","gas":1695},{"name":"ticketCount","outputs":[{"type":"uint256","name":""}],"inputs":[{"type":"address","name":"arg0"}],"stateMutability":"view","type":"function","gas":1725},{"name":"enemyServants","outputs":[{"type":"tuple","name":"cl","components":[{"type":"string","name":"name"}]},{"type":"uint256","name":"level"},{"type":"uint256","name":"atk"},{"type":"uint256","name":"hp"}],"inputs":[{"type":"uint256","name":"arg0"}],"stateMutability":"view","type":"function","gas":10859},{"name":"recordIndex","outputs":[{"type":"uint256","name":""}],"inputs":[],"stateMutability":"view","type":"function","gas":1631},{"name":"recordbook","outputs":[{"type":"address","name":"player"},{"type":"string","name":"pClass"},{"type":"string","name":"eClass"},{"type":"bool","name":"result"}],"inputs":[{"type":"uint256","name":"arg0"}],"stateMutability":"view","type":"function","gas":16185}];

var Gacha = new web3.eth.Contract(GachaABI,'0xe9D7905041Ef8480aAbCb350fCd300a66Fd9C1B1');

/* ================================================================================*/
/* Update the UI with current wallet account details when called */
async function updateAccount() {
  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  const accountNode = document.getElementById("account");
  if (accountNode.firstChild)
    accountNode.firstChild.remove();
  var textnode = document.createTextNode(account);
  accountNode.appendChild(textnode);

  const ticketNode = document.getElementById("tickets");
  if(ticketNode.firstChild)
    ticketNode.firstChild.remove();
  const ticketNum = await Gacha.methods.ticketCount(account).call();
  var ticketNumNode = document.createTextNode(ticketNum);
  ticketNode.appendChild(ticketNumNode);
}

/* ================================================================================*/
/* Update the UI with account's servant details when called */
async function displayServants() {
  const servantsNode = document.getElementById("servantList");
  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  while (servantsNode.firstChild) {
    servantsNode.firstChild.remove();
  }
  var servantCount = await Gacha.methods.servantCount(account).call();
  for (var i = 0; i < servantCount; i++){
    var servant = await Gacha.methods.servants(account, i).call();
    const className = document.createTextNode("Class: " + servant.cl.name);
    const level = document.createTextNode("Level: " + servant.level);
    const atk = document.createTextNode("Attack: " + servant.atk);
    const hp = document.createTextNode("HP: " + servant.hp);
    const li = document.createElement("li");
    const br1 = document.createElement("br");
    const br2 = document.createElement("br");
    const br3 = document.createElement("br");

    li.classList.add("servant");
    li.appendChild(className);
    li.appendChild(br1);
    li.appendChild(level);
    li.appendChild(br2);
    li.appendChild(atk);
    li.appendChild(br3);
    li.appendChild(hp);

    servantsNode.appendChild(li);
  }
}

/* ================================================================================*/
/* Update the UI with enemy servant details when called */
async function displayEnemies() {
  const enemiesNode = document.getElementById("enemyList");
  while (enemiesNode.firstChild) {
    enemiesNode.firstChild.remove();
  }
  for (var i = 0; i < 4; i++){
    var servant = await Gacha.methods.enemyServants(i).call();
    const className = document.createTextNode("Class: " + servant.cl.name);
    const level = document.createTextNode("Level: " + servant.level);
    const atk = document.createTextNode("Attack: " + servant.atk);
    const hp = document.createTextNode("HP: " + servant.hp);
    const li = document.createElement("li");
    const br1 = document.createElement("br");
    const br2 = document.createElement("br");
    const br3 = document.createElement("br");

    li.classList.add("servant");
    li.appendChild(className);
    li.appendChild(br1);
    li.appendChild(level);
    li.appendChild(br2);
    li.appendChild(atk);
    li.appendChild(br3);
    li.appendChild(hp);

    enemiesNode.appendChild(li);
  }
}

/* ================================================================================*/
/* Update the UI with fight entries from contract when called */
async function updateEntries(){
  const entriesNode = document.getElementById("entries");

  while (entriesNode.firstChild) {
    entriesNode.firstChild.remove();
  }

  for (var i = 0 ; i < 3; i++) {
      var entry = await Gacha.methods.recordbook(i).call();
      const wallet = document.createTextNode(entry.player);
      var fightResult;
      if (entry.result){
        fightResult = " won.";
      }
      else{
        fightResult = " lost.";
      }
      const message = document.createTextNode(entry.pClass + " fought " + entry.eClass + " and" + fightResult);
      const br1 = document.createElement("br");
      const p = document.createElement("p");

      p.classList.add("entry");
      p.appendChild(wallet);
      p.appendChild(br1);
      p.appendChild(message);

      entriesNode.appendChild(p);
  }
};

/* Issue a transaction to fight based on form field values */
async function fight() {
  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  const pRosterNum = document.getElementById("pRosterNum").value - 1;
  const eRosterNum = document.getElementById("eRosterNum").value - 1;
  const transactionParameters = {
	  from: account,
	  gasPrice: 0x1D91CA3600,
	  value: 0
  };
  await Gacha.methods.fight(pRosterNum, eRosterNum).send(transactionParameters);
};

/* Create submission button.  Then, register an event listener on it to invoke fight
* transaction when clicked */
const fightButton = document.getElementById('fight');
fightButton.addEventListener('click', () => {
  fight();
});

/* Issue a transaction to buy tickets based on form field values */
async function buyTickets() {
  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  const ticketNum = document.getElementById("ticketNum").value;
  const transactionParameters = {
    from: account,
    gasPrice: 0x1D91CA3600,
    value: ticketNum * 500
  };
  await Gacha.methods.buyTicket(ticketNum).send(transactionParameters);
};

/* Create submission button.  Then, register an event listener on it to invoke ticket
* transaction when clicked */
const ticketButton = document.getElementById('buyTickets');
ticketButton.addEventListener('click', () => {
  buyTickets();
});

/* Issue a transaction to summon based on form field values */
async function summon() {
  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  const ticketNum = document.getElementById("ticketNum").value;
  const transactionParameters = {
    from: account,
    gasPrice: 0x1D91CA3600,
    value: 0
  };
  await Gacha.methods.summon().send(transactionParameters);
};

/* Create submission button.  Then, register an event listener on it to invoke summon
* transaction when clicked */
const summonButton = document.getElementById('summon');
summonButton.addEventListener('click', () => {
  summon();
});
