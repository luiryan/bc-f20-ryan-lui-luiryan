# @version ^0.2.4

# struct to fake list of strings
struct className:
    name: String[10]

# owner address    
owner: public(address)

# Class parameters
NUM_CLASSES: constant(uint256) = 4
classList: public(className[NUM_CLASSES])

# player parameters
MAX_PLAYERS: constant(uint256) = 10
currPlayers: public(uint256)
playerList: public(address[10])
cooldown: public(HashMap[address, uint256])

# servant parameters
BASE_ATK: constant(uint256) = 100
BASE_HP: constant(uint256) = 1000

# servant struct
struct Servant:
    cl: className
    level: uint256
    atk: uint256
    hp: uint256

# tracking servants
servants: public(HashMap[address, Servant[NUM_CLASSES]]) 
servantCount: public(HashMap[address, uint256])
ticketCount: public(HashMap[address, uint256])
MAX_ENEMY_SERVANTS: constant(uint256) = NUM_CLASSES
enemyServants: public(Servant[MAX_ENEMY_SERVANTS])

# track win/loss
struct Record:
    player: address
    pClass: String[10]
    eClass: String[10]
    result: bool

recordIndex: public(uint256)
MAX_RECORDS: constant(uint256) = 3
recordbook: public(Record[MAX_RECORDS])

# constructor
@external
def __init__():
    # initialize classes
    self.classList[0].name = "Saber"
    self.classList[1].name = "Archer"
    self.classList[2].name = "Lancer"
    self.classList[3].name = "Berserker"
    # initialize enemy servants
    self.enemyServants[0] =  Servant({
        cl: self.classList[0],
        level: 1,
        atk: (block.timestamp % BASE_ATK) + BASE_ATK,
        hp: (block.timestamp % BASE_HP) + BASE_HP
        })
    self.enemyServants[1] =  Servant({
        cl: self.classList[1],
        level: 5,
        atk: (block.timestamp % BASE_ATK) * 5 + BASE_ATK,
        hp: (block.timestamp % BASE_HP) * 5 + BASE_HP
        })
    self.enemyServants[2] =  Servant({
        cl: self.classList[2],
        level: 10,
        atk: (block.timestamp % BASE_ATK) * 10 + BASE_ATK,
        hp: (block.timestamp % BASE_HP) * 10 + BASE_HP
        })
    self.enemyServants[3] =  Servant({
        cl: self.classList[3],
        level: 20,
        atk: (block.timestamp % BASE_ATK) * 20 + BASE_ATK,
        hp: (block.timestamp % BASE_HP) * 20 + BASE_HP
        })
    # set owner as first player
    self.owner = msg.sender
    self.currPlayers = 1
    self.playerList[0] = msg.sender
    self.servantCount [msg.sender] = 0
    self.ticketCount[msg.sender] = 1
    self.cooldown[msg.sender] = 0
    # initialize recordbook
    self.recordIndex = MAX_RECORDS
    for i in range(MAX_RECORDS):
        self.recordbook[i] = Record({
            player: empty(address),
            pClass: "empty",
            eClass: "empty",
            result: False
        })

# check if an address is registered 
@internal
def isRegistered(addr: address) -> bool:
    ret: bool = False
    for i in range(MAX_PLAYERS):
        if addr == self.playerList[i]:
            ret = True
    return ret

# register to play
@external
@payable
def register():
    assert msg.value == 1000, "1000 wei required to register"
    assert self.currPlayers < MAX_PLAYERS, "maximum number of players already registered"
    self.playerList[self.currPlayers] = msg.sender
    self.currPlayers += 1
    self.servantCount[msg.sender] = 0
    self.ticketCount[msg.sender] = 1
    self.cooldown[msg.sender] = 0

# purchase tickets
@external
@payable
def buyTicket(tickets: uint256):
    assert self.isRegistered(msg.sender), "you are not registered to play"
    # each ticket costs 500 wei
    assert msg.value == tickets * 500, "payment does not match ticket count"
    self.ticketCount[msg.sender] += tickets

# use ticket to summon a new servant
@external
def summon():
    assert self.isRegistered(msg.sender), "you are not registered to play"
    assert self.ticketCount[msg.sender] > 0, "you have no tickets remaining"
    assert self.servantCount[msg.sender] < NUM_CLASSES, "you already have the maximum number of servants"
    self.ticketCount[msg.sender] -= 1
    self.servantCount[msg.sender] += 1
    classNum: uint256 = block.timestamp % NUM_CLASSES
    self.servants[msg.sender][self.servantCount[msg.sender]-1] = Servant({
        cl: self.classList[classNum],
        level: 1,
        atk: (block.timestamp % BASE_ATK) + BASE_ATK,
        hp: (block.timestamp % BASE_HP) + BASE_HP
        })

# level up the servant in slot playerServantNum for player with address addr
#   by the provided number of levels
@internal
def _levelUp(playerServantNum: uint256, addr: address, levels: uint256):
    self.servants[addr][playerServantNum].level += levels
    self.servants[addr][playerServantNum].atk += (block.timestamp % BASE_ATK) * levels
    self.servants[addr][playerServantNum].hp += (block.timestamp % BASE_HP) * levels

# write the fight's results into the public recordbook
# requires player's address, player's and enemy servant's class as strings, and the result 
#   of the fight as a bool
@internal
def _recordFight(pAddr: address, playerClass: String[10], enemyClass: String[10], res: bool):
    newRecord: Record = Record({
        player: pAddr,
        pClass: playerClass,
        eClass: enemyClass,
        result: res
    })
    if self.recordIndex < MAX_RECORDS - 1:
        self.recordIndex += 1
    else:
        self.recordIndex = 0
    self.recordbook[self.recordIndex] = newRecord

# fight another servant
# playerServantNum: player's servant in roster to begin fight
# enemyServantNum:  enemy servant in enemy roster to be fought
@external
def fight(playerServantNum: uint256, enemyServantNum: uint256):
    assert self.isRegistered(msg.sender), "you are not registered to play"
    # initial default value of atk (as a uint256) should be 1, so check if it is still default
    assert 0 <= playerServantNum and playerServantNum < self.servantCount[msg.sender] and self.servants[msg.sender][playerServantNum].atk > 1, "no such servant available"
    assert 0 <= enemyServantNum and enemyServantNum < MAX_ENEMY_SERVANTS and self.enemyServants[enemyServantNum].atk > 1, "no such enemy servant"
    # players can only fight once per hour
    assert self.cooldown[msg.sender] <= block.timestamp, "you are still on cooldown. you can only fight once per hour"
    self.cooldown[msg.sender] = block.timestamp + 3600
    playerServant: Servant = self.servants[msg.sender][playerServantNum]
    enemyServant: Servant = self.enemyServants[enemyServantNum]
    # damage modifier logic
    # (Saber > Archer > Lancer > Saber) <> Berserker
    #   self.classList[0].name = "Saber"
    #   self.classList[1].name = "Archer"
    #   self.classList[2].name = "Lancer"
    #   self.classList[3].name = "Berserker"
    pAtkMod: decimal = 1.0
    eAtkMod: decimal = 1.0
    # if player saber
    if playerServant.cl.name == self.classList[0].name:   
        # if enemy archer
        if enemyServant.cl.name == self.classList[1].name: 
            pAtkMod = 0.5
            eAtkMod = 2.0
        # else if enemy lancer
        elif enemyServant.cl.name == self.classList[2].name:
            pAtkMod = 2.0
            eAtkMod = 0.5
        # else if enemy is berserker
        elif enemyServant.cl.name == self.classList[3].name:  
            pAtkMod = 2.0
            eAtkMod = 1.5 
        # else deal neutral damage
    # else if player archer
    elif playerServant.cl.name == self.classList[1].name:   
        # if enemy saber
        if enemyServant.cl.name == self.classList[0].name: 
            pAtkMod = 2.0
            eAtkMod = 0.5
        # else if enemy lancer
        elif enemyServant.cl.name == self.classList[2].name:
            pAtkMod = 0.5
            eAtkMod = 2.0
        # else if enemy is berserker
        elif enemyServant.cl.name == self.classList[3].name:  
            pAtkMod = 2.0
            eAtkMod = 1.5 
        # else deal neutral damage
    # else if player lancer
    elif playerServant.cl.name == self.classList[2].name:   
        # if enemy saber
        if enemyServant.cl.name == self.classList[0].name: 
            pAtkMod = 0.5
            eAtkMod = 2.0
        # else if enemy archer
        elif enemyServant.cl.name == self.classList[1].name:
            pAtkMod = 2.0
            eAtkMod = 0.5
        # else if enemy is berserker
        elif enemyServant.cl.name == self.classList[3].name:  
            pAtkMod = 2.0
            eAtkMod = 1.5 
        # else deal neutral damage
    # else if player berserker
    elif playerServant.cl.name == self.classList[2].name:   
        # if enemy berserker
        if enemyServant.cl.name == self.classList[3].name: 
            pAtkMod = 1.5
            eAtkMod = 1.5
        # else deal deal 1.5x take 2x
        else:
            pAtkMod = 1.5
            eAtkMod = 2.0
    # do the actual fight. each servant gets one attack in; first the attacker, then the defender.
    # each attack is modified by the modifier calculated depending on the class combination
    # if defender has 0 or fewer hp after the first attack, attacker wins.
    # otherwise, the defender gets their attack in and whoever has the highest hp afterwards wins.
    wonFight: bool = False
    newEnemyHp: uint256 = enemyServant.hp - convert(floor(convert(playerServant.atk, decimal) * pAtkMod), uint256)
    newPlayerHp: uint256 = playerServant.hp - convert(floor(convert(enemyServant.atk, decimal) * eAtkMod), uint256)
    if newEnemyHp <= 0 or newEnemyHp < newPlayerHp:
        wonFight = True
    # if the player wins, their servant gets to level up 3 times, otherwise they level up only once
    if wonFight:
        self._levelUp(playerServantNum, msg.sender, 3)
    else:
        self._levelUp(playerServantNum, msg.sender, 1)
    # record fight in recordbook
    self._recordFight(msg.sender, playerServant.cl.name, enemyServant.cl.name, wonFight)